# Set up
## Server side

```bash
$ npm i
$ nodemon
```
Stack:  Node, Express, MongoDB, GraphQL
to visualise graphQl please visite http://localhost:3040/graphql

## Client side
```bash
$ npm i
$ npm start
```

Stack:  React, Apollo