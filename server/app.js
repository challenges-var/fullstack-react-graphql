const express = require('express')
const graphqlHTTP = require('express-graphql')

const schema = require('./schema/schema')
const mongoose = require('mongoose')

const cors = require('cors')

const app = express()
const port = 3040

// Allow cross origin requests
app.use(cors())

app.use(express.static('public'))

mongoose.connect('mongodb://timo:test@ds219879.mlab.com:19879/gql-test')
mongoose.connection.once('open', () => {
  console.log('Connected to DB!')
})

// Middleware for GraphQL
app.use('/graphql', graphqlHTTP({
  schema,
  graphiql: true
}))

app.listen(port, () => {
  console.log(`Your app is runnig on http://localhost:${port}`)
})